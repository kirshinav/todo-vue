import Vue from "vue";
import Vuex from "vuex";
import VueResource from "vue-resource";

Vue.use(Vuex);
Vue.use(VueResource);

export default new Vuex.Store({
    state: {
        db: Vue.resource('http://localhost:3000/notes/'),
        notes: [],
        preparedNotes: [],
        dialog: {
            show: false,
            title: '',
            type: ''
        }
    },
    mutations: {
        openDialog (state, payload) {
            state.dialog.show = true;
            state.dialog.title = payload.title;
            state.dialog.type = payload.type
        },
        getNotesFromDB (state) {
            state.db.get()
                .then(response => response.json())
                .then(newResponse => state.notes = newResponse.reverse())
        },
        setActiveNote (state, payload) {
            state.activeNote = state.notes.find(e => e.id === payload)
        },
        clearActiveNote (state) {
            state.activeNote = {}
        },
        createNote (state, payload) {
            state.db.save(payload)
        },
        createTask (state, payload) {
            state.activeNote.tasks.push(payload)
        },
        pushToPreparedNotes (state, payload) {
            state.preparedNotes.push(payload)
        }
        // changeDone (state, payload) {
        //     state.activeNote.tasks[payload.taskIndex].isDone = payload.isDone
        // },
        // putChangesToDB (state) {
        //     state.db.update(state.notes)
        //         .then(response => {
        //             console.log(response)
        //         })
        // }
    },
    getters: {
        getNotes (state) {
            return state.notes
        },
        getActiveNote (state) {
            return state.activeNote
        },
        getNoteById: state => id => {
            return state.notes.find(e => e.id === id)
        }
    }
})
