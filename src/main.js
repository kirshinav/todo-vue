import Vue from "vue";
import App from "./App.vue";
import VueRouter from "vue-router";
import router from "./routes";
import VueResource from "vue-resource";
import store from "./store";

Vue.use(VueRouter);
Vue.use(VueResource);

new Vue({
    el: '#app',
    store,
    render: h => h(App),
    router
});
