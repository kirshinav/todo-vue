import VueRouter from "vue-router";
import Notes from "./pages/Notes";
import NoteFull from "./pages/NoteFull";
import NoteNew from "./components/NoteNew";

export default new VueRouter({
    routes: [
        {
            path: '',
            redirect: '/notes'
        },
        {
            path: '*',
            redirect: '/notes'
        },
        {
            path: '/notes',
            component: Notes
        },
        {
            path: '/notes/:id',
            component: NoteFull,
            name: 'noteLink'
        },
        {
            path: '/new',
            component: NoteNew
        }
    ],
    mode: 'history'
})
